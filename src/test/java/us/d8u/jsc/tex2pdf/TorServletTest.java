package us.d8u.jsc.tex2pdf;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.Header;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpHost;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.AuthCache;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpHead;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.BasicAuthCache;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.ContentEncodingHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicHeader;
import org.apache.http.util.EntityUtils;
import org.joda.time.DateTime;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TorServletTest { 
  private final static Logger LOG = LoggerFactory.getLogger(us.d8u.jsc.tex2pdf.TorServletTest.class);
	@Test
	public void infoTest() {
		HttpHost target = new HttpHost("localhost", 8080, "http");
		CredentialsProvider credsProvider = new BasicCredentialsProvider();
		credsProvider.setCredentials(
				                new AuthScope(target.getHostName(), target.getPort()),
												                new UsernamePasswordCredentials("hdiwan", "Wonju777"));
		CloseableHttpClient httpclient = HttpClients.custom()
			                 .setDefaultCredentialsProvider(credsProvider).build();
		 try {
			// Create AuthCache instance
			AuthCache authCache = new BasicAuthCache();
			// Generate BASIC scheme object and add it to the local
			// auth cache
			BasicScheme basicAuth = new BasicScheme();
			authCache.put(target, basicAuth);
			
			// Add AuthCache to the execution context
			HttpClientContext localContext = HttpClientContext.create();
			localContext.setAuthCache(authCache);
			HttpGet httpget = new HttpGet("http://httpbin.org/hidden-basic-auth/user/passwd");
			CloseableHttpResponse response = httpclient.execute(target, httpget, localContext);
			assertThat("Response Code", response.getStatusLine().getStatusCode(), is(200));
		} catch (Exception t) { 
			LOG.error(t.getMessage(), t);
			fail(t.getMessage());
		}
	}
	
	@Test
	public void testGzipHandler() {
		String url = "http://localhost:8080/info";
    HttpHost target = new HttpHost("localhost", 8080, "http");                                                                
		CredentialsProvider credsProvider = new BasicCredentialsProvider();                                                       
		credsProvider.setCredentials(new AuthScope(target.getHostName(), target.getPort()), new UsernamePasswordCredentials("hdiwan", "Wonju777"));                               
		Header header = new BasicHeader(HttpHeaders.CONTENT_TYPE, "application/json");
		List<Header> headers = new ArrayList<>();
		headers.add(header);
		// Create AuthCache instance
		AuthCache authCache = new BasicAuthCache();
		// Generate BASIC scheme object and add it to the local
		// auth cache       
		BasicScheme basicAuth = new BasicScheme();
		authCache.put(target, basicAuth);
		
		// Add AuthCache to the execution context         
		HttpClientContext localContext = HttpClientContext.create();
		localContext.setAuthCache(authCache);             

		CloseableHttpClient httpclient = HttpClients.custom().setDefaultCredentialsProvider(credsProvider).setDefaultHeaders(headers).build();
		HttpGet method = new HttpGet(url);
		CloseableHttpResponse response = null;
		try {
			response = httpclient.execute(target, method, localContext);
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			fail(e.getMessage());
		}
		String encoding = response.getEntity().getContentEncoding().getValue();
		LOG.info("encoding is "+encoding);
		assertThat("Content-Encoding Header", encoding, is("gzip"));
	}

	@Test
	// check if its saved
	public void checkRemote() {
		HttpHead httpHead = null;
		try {
			CloseableHttpClient httpClient = HttpClients.createMinimal();
			httpHead = new HttpHead("http://hasan.d8u.us/torhosts.json");
			httpClient.execute(httpHead);
		} catch (Throwable t) {
			LOG.error(t.getMessage(), t);
			fail(t.getMessage());
		}
		DateTime modifiedShouldBe = new DateTime();
		DateTime modifiedAt = new DateTime(Long.parseLong(httpHead.getFirstHeader("Last-Modified").getValue()));
		assertThat("Upload worked", modifiedAt, is(modifiedShouldBe));
	}
}
