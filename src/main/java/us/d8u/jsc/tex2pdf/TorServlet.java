package us.d8u.jsc.tex2pdf;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.InputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.json.JettisonMappedXmlDriver;
import org.apache.tools.ant.Project;
import org.apache.tools.ant.ProjectHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TorServlet extends HttpServlet {
	private final static Logger LOGGER = LoggerFactory.getLogger(us.d8u.jsc.tex2pdf.TorServlet.class);
	private final static String BUILD_FILE_LOCATION = "src/main/resources/build.xml";

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Map<String, String> ret = new HashMap<>();
		String torHost = request.getParameter("hostname");
		BufferedReader reader = null;
		FileReader configuration = new FileReader("/usr/local/etc/tor/torrc");
		reader = new BufferedReader(configuration);
		XStream xstream = new XStream(new JettisonMappedXmlDriver());
		xstream.setMode(XStream.NO_REFERENCES);
		String line;
		try {
			FileReader wrappedReader = new FileReader(String.format("/var/db/tor/%s/hostname", torHost));
			reader = new BufferedReader(wrappedReader);
			String hostname = reader.readLine();
			ret.put("hostname", hostname);
		} catch (FileNotFoundException e) {
		} finally {
				while ((line = reader.readLine()) != null) {
					if (line.startsWith("HiddenServiceDir ")) {
						String hostkey = line.split(" ")[1].replace("/var/db/tor/", "");
						String hostname = new BufferedReader(new FileReader(String.format("%s/hostname", line.split(" ")[1]))).readLine();
						ret.put(hostkey.substring(0, hostkey.length()-1), hostname);
					}
			}
		}
		response.setContentType("application/json");
		response.getWriter().write(xstream.toXML(ret));
		moveToRemote(ret);
		reader.close(); 
	}

	public static void moveToRemote(Map<String, String> ret) {
		try {
			final File temporaryJsonFile = File.createTempFile(new Long(System.currentTimeMillis()).toString(), ".json", new File("."));
			BufferedOutputStream temporaryJsonFileOutputStream = new BufferedOutputStream(new FileOutputStream(temporaryJsonFile));
			temporaryJsonFileOutputStream.write(new XStream(new JettisonMappedXmlDriver()).toXML(ret).getBytes());
			temporaryJsonFileOutputStream.close();
			System.setProperty("json", temporaryJsonFile.getAbsolutePath());
			// invoke ant task to put the file on the remote host
			File buildFile = new File("src/main/resources/build.xml");
			Project p = new Project();
			p.setUserProperty("ant.file", buildFile.getAbsolutePath());
			p.init();
			ProjectHelper helper = ProjectHelper.getProjectHelper();
			p.addReference("ant.projectHelper", helper);
			helper.parse(p, buildFile);
			LOGGER.info("Default target is "+p.getDefaultTarget());
			p.executeTarget(p.getDefaultTarget());
		}	catch (Exception e) { 
			LOGGER.error("Runtime Exception when invoking ant", e); 
		}
	}
	
	public static void main(String[] args) throws Throwable { }
}
