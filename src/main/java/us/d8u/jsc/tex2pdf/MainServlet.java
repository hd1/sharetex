package us.d8u.jsc.tex2pdf;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FilenameFilter;
import java.io.InputStreamReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.json.JettisonMappedXmlDriver;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MainServlet extends HttpServlet {
	private final static Logger LOGGER = LoggerFactory.getLogger(us.d8u.jsc.tex2pdf.MainServlet.class);
	public final static String HOME_JSP = "/home.jsp";
  public String[] getTeXOptions() {
    File directory = new File("/usr/home/hdiwan/tex");
    String[] LaTeXFiles = directory.list(new FilenameFilter() {
	    public boolean accept(File dir, String name) {
 	      return name.endsWith(".tex");
		 	}
	 	}); 
	 	return LaTeXFiles;
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		Map<String, String> map = new HashMap<>();
		for (String filename : getTeXOptions()) {
			map.put(filename, "true");
		}
		Set<String> latexFiles = map.keySet();

		response.setStatus(HttpServletResponse.SC_OK);
		if (request.getParameter("filename") != null) {
			String target = request.getParameter("filename").substring(0, request.getParameter("filename").indexOf(".tex"));
			String finalPath = String.format("%s/public_html/%s.pdf", System.getProperty("user.home"), target);
			Process p = new ProcessBuilder("/usr/bin/make", "MAIN="+target, "publish").directory(new File("/home/hdiwan/tex")).redirectErrorStream(true).start();
			String status = "";
			try(BufferedReader in = new BufferedReader(new InputStreamReader(p.getInputStream()))) {
				String line;
			 	while ((line = in.readLine()) != null) {
					status = status + line + "\n";
				}
			}
			byte[] contentAsBytes = new byte[Math.toIntExact(new File(finalPath).length())];
			FileInputStream stream = new FileInputStream(finalPath);
			stream.read(contentAsBytes);
			stream.close();
			new File(String.format("%s.pdf", target)).delete();
			response.setContentType("application/pdf");
			response.getOutputStream().write(contentAsBytes);
		} else {
			response.setContentType("application/json");
			XStream xstream = new XStream(new JettisonMappedXmlDriver());
			response.getWriter().println(xstream.toXML(latexFiles));
		}
	}

	public static void main(String[] args) {
		LOGGER.error("testing slf4j");
 	}

}	
